﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubePool : MonoBehaviour
{
    public int maxCubesCount;
    public GameObject cubePrefab;

    public Transform player;
    public int spawnRadius;
    public float spawnTime;

    private List<GameObject> cubes = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < maxCubesCount; i++)
        {
            GameObject newCube = Instantiate(cubePrefab);
            newCube.transform.SetParent(gameObject.transform);
            newCube.SetActive(false);
            cubes.Add(newCube);
        }

        StartCoroutine("SpawnNewCubes");
    }

    IEnumerator SpawnNewCubes()
    {
        while(true)
        {
            yield return new WaitForSeconds(spawnTime);
            foreach (var cube in cubes)
            {
                if (!cube.activeSelf)
                {
                    cube.SetActive(true);

                    int randomX = Random.Range(-spawnRadius, spawnRadius);
                    int randomZ = Random.Range(-spawnRadius, spawnRadius);

                    Vector3 spawnPosition = player.position + new Vector3(randomX, 0, randomZ);
                    cube.transform.position = spawnPosition;
                    break;
                }
                
                // Cube can spawn in empty space when player around walls
                if(cube.transform.position.y < 0)
                {
                    cube.SetActive(true);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}